import Fuse from "fuse.js";

const fuseOptions = {
  shouldSort: true,
  includeMatches: true,
  threshold: 0.0,
  tokenize: true,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 3,
  keys: [{ name: "title", weight: 0.8 }, { name: "body", weight: 0.5 }]
};

// Mute the fuse options object.
Object.freeze(fuseOptions);

const container = document.getElementById("search-results");

// Check for query params.
const urlParams = new URLSearchParams(window.location.search);
const query = urlParams.get("s");

if (query) {
  fetch("/search/index.json")
    .then(response => response.json())
    .then(body => {
      const fuse = new Fuse(body, fuseOptions);
      const results = fuse.search(query);

      // Display query on page.
      const searchQueryElement = document.getElementById("search-query");
      searchQueryElement.textContent = query;

      if (results.length) {
        // Output our results.
        for (const result of results) {
          // Construct container element for the result.
          const element = document.createElement("div");
          element.className = "mb4 bb b-rich-black";

          // Construct title
          const title = document.createElement("a");
          title.className =
            "b no-underline  rich-black hover-michigan-blue underline-hover f3";
          const titleText = document.createTextNode(result.item.title);

          title.href = result.item.permalink;
          title.appendChild(titleText);

          element.appendChild(title);

          // Construct body content.
          const body = document.createElement("p");
          const bodyString = result.item.body;
          const trimmedBody = document.createTextNode(
            bodyString.substr(0, 200).trim() + "..."
          );

          body.appendChild(trimmedBody);

          element.appendChild(body);

          // Added our element to the container.
          container.appendChild(element);
        }
      } else {
        const noResults = document.createElement("p");
        const noResultsText = "No results found, please try again.";

        noResults.textContent = noResultsText;
        container.appendChild(noResults);
      }
    });
}
