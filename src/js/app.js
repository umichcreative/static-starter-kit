import lozad from "lozad";
import "intersection-observer";

// Check that service workers are registered
if ("serviceWorker" in navigator) {
  // Use the window load event to keep the page load performant
  window.addEventListener("load", () => {
    const serviceWorker = "/sw.js";
    navigator.serviceWorker.register(serviceWorker);
  });
}

// Lozad all the things.
const observer = lozad();
observer.observe();

// Dynamic imports.
(async () => {
  // Load search JS if proper element is detected.
  const searchResults = document.getElementById("search-results");

  if (searchResults) {
    await import("./search.js");
  }
})();
