module.exports = {
  plugins: {
    "@fullhuman/postcss-purgecss": { content: ["./site/layouts/**/*.html"] },
    "postcss-preset-env": { stage: 1 },
    "autoprefixer": {}
  }
};
