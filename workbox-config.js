module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{html,yml,js,png,css,ico,svg,jpg,json,woff2,woff,xml}"
  ],
  "swDest": "dist/sw.js"
};