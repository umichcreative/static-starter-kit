---
title: Static Starter Kit
hero:
  heading: Mission Statement Purus Nibh Elit Adipiscing
  image: img/hero.jpg
  text: Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis posuere velit aliquet.
  cta_text: Nullam ornare eu leo
  cta_link: /
cards:
  - heading: Card title
    text: Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Cras justo odio dapibus ac.
    link_url: /
    link_text: Card Link
    image: img/card.jpg
    image_alt: card image
  - heading: Card title
    text: Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Cras justo odio dapibus ac.
    link_url: /
    link_text: Card Link
    image: img/card.jpg
    image_alt: card image
  - heading: Card title
    text: Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Cras justo odio dapibus ac.
    link_url: /
    link_text: Card Link
    image: img/card.jpg
    image_alt: card image
mission_statement:
  text: Here is some mission statement stuff...
infographics:
  infographic:
    - large_text: Large Text
      link_text: Push this button
      link_url: 'https://www.google.com'
      small_text: Small Text
featured_section:
  feature:
    - image: /uploads/logo.png
      link_text: Stuff
      link_url: Stuff
      text: Stuff
  heading: Stuff
seo:
  meta_description: Stuff
---

